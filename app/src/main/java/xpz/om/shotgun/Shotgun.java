package xpz.om.shotgun;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;
import static de.robv.android.xposed.XposedHelpers.findClass;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class Shotgun implements IXposedHookLoadPackage {
    // Write to the Xposed log only in DEBUG mode
    private static void ShotgunPrint(String log)
    {
        if (BuildConfig.DEBUG) {
            XposedBridge.log(log);
        }
    }

    public void handleLoadPackage(final LoadPackageParam lpparam) throws Throwable {
        if (!lpparam.packageName.equals("com.waze"))
            return;

        // Create a dictionary of all methods we need to cancel and their single parameter type
        Map<String, Object> hooks = new HashMap<>();
        hooks.put("EditBoxCheckTypingLock", EditText.class);
        hooks.put("EditBoxCheckTypingLockCb", int.class);
        hooks.put("checkTypingLock", findClass("com.waze.view.text.a", lpparam.classLoader));

        // Iterate the dictionary and hook all the methods in order to cancel them
        for (final Map.Entry<String, Object> x : hooks.entrySet())
        {
            findAndHookMethod("com.waze.NativeManager", lpparam.classLoader, x.getKey(), x.getValue(), new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    ShotgunPrint(String.format("Canceled method %s", x.getKey()));
                    param.setResult(null);
                }
            });

            ShotgunPrint(String.format("Waze shotgun hooked %s successfully!", x.getKey()));
        }
    }
}
